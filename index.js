const http = require('http');
const https = require('https');
const fs = require('fs');
const express = require('express');
const cookieparser = require('cookie-parser');
const session = require('express-session');
const QQDBCtrller = require('./QQDBCtrller.js');

const server_name = 'somethingwas.tw';
const port = 80;
const ports = 443;
const ssl = {
    'key': 'fs.readFileSync(/etc/path/to/privkey.pem)',
    'cert': 'fs.readFileSync(/etc/path/to/cert.pem)',
    'ca': 'fs.readFileSync(/etc/path/to/ca.pem)'
};

const qqdb = new QQDBCtrller();

// const apps = express();
// https.createServer(ssl, app).listen(ports, () => {
//     console.log(now(), '[SV] https server start.');
// });

const app = express();
http.createServer(app).listen(port, () => { console.log(now(), '[SV] http server start.'); });

const sessStore = new session.MemoryStore();
const sessMW = session({
    secret: 'recommand 128 bytes random string',
    resave: false,
    saveUninitialized: true,
    store: sessStore,
    cookie: {
        path: '/api/qq',
        maxAge: 60 * 60 * 1000
        // secure: true
    }
});

app.disable('x-powered-by');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieparser());
app.use(sessMW);

app.use('/asset', express.static(__dirname + '/asset'));
app.use('/pad-ss/asset', express.static(__dirname + '/pad-ss/asset'));

app.use((req, res, next) => {
    let method = req.method;
    let subdomain = req.hostname;
    let path = req.path;
    let ip = ipdefined(req.ip);
    if (subdomain == 'api.' + server_name) {
        req.url = '/api' + req.url;
        req.header['x-subdomain'] = 'api';
    }
    console.log(now(), '[SV] Req', method, path, '(' + ip + ')');
    console.log('        url:', req.url);
    if (method == 'POST') {
        console.log('        body:', req.body);
    }
    next();
});

app.get('/', (req, res) => {
    res.send('Hello, world!?87QQ');
});

app.get('/resume', (req, res) => {
    res.sendFile('resume.html', {'root': __dirname});
});

app.get('/pad-ss', (req, res) => {
    res.sendFile('/pad-ss/index.html', {'root': __dirname});
});

app.route('/api/qq:path(/?*)')
.get((req, res) => { // web side page
    if (!req.params.path || req.params.path == '/') {
        if (!req.session.authorization) {
            res.redirect(401, '/api/qq/login');
        } else {
            res.sendFile('QQAdmin.html', {'root': __dirname});
        }
    } else if (req.params.path == '/login') {
        res.sendFile('QQLogin.html', {'root': __dirname});
    } else {
        res.status(404).end();
    }
})
.post((req, res) => {
    if (req.params.path == '/login') { // web side login
        let un = req.body.username, pw = req.body.password, ca = req.body.captcha;
        if (un == pw && pw.slice(0, -1) == ca) {
            req.session.username = un;
            req.session.authorization = true;
            req.session.IP = ipdefined(req.ip);
            res.type('application/json');
            res.send({
                'result': true,
                'apipath': req.path,
                'errorMessage': ''
            });
        } else {
            res.type('application/json');
            res.status(401).send({
                'result': false,
                'apipath': req.path,
                'errorMessage': 'Unauthorized'
            });
        }
    } else if (req.params.path == '/logout') { // api side
        if (req.session.authorization) {
            req.session.destroy();
            res.send('logout');
        } else {
            res.send('error');
        }
    } else if (!req.params.path || req.params.path == '/') { // api side
        res.header('Access-Control-Allow-Origin', '*');
        res.type('application/json');
        // if (!req.session.authorization) {
        //     res.status(403).send({
        //         'result': false,
        //         'apipath': req.path,
        //         'errorMessage': 'Unauthorized'
        //     });
        if (!req.body.key) {
            res.send({
                'result': false,
                'apipath': req.path,
                'errorMessage': 'no keys found',
                'action': req.body.key,
                'response': {}
            });
        } else {
            let keys = req.body.key.split('/');
            if (keys[0] == 'index') {
                let page = keys[1] || 1;
                Promise.all([
                    qqdb.getListPage(),
                    qqdb.getList(page)
                ]).then(([MaxPage, Row]) => {
                    res.send({
                        'result': true,
                        'apipath': req.path,
                        'errorMessage': '',
                        'action': req.body.key,
                        'response': {
                            'nowPage': page,
                            'maxPage': MaxPage,
                            'gameList': Row
                        }
                    });
                });
            } else {
                res.send({
                    'result': false,
                    'apipath': req.path,
                    'errorMessage': 'invalid keys',
                    'action': req.body.key,
                    'response': {}
                });
            }
            // res.send({
            //     "result": true,
            //     "resultCode": 200,
            //     "apipath": "api/mian/getGameList",
            //     "errorMessage": "",
            //     "action": "",
            //     "reaponse": {
            //         "nowPage": 1,
            //         "maxPage":3,
            //         "gameList": [{
            //             "index": 782193789121,
            //             "name":"AWALONG",
            //             "englishName":"sadasdas",
            //             "images": ["/images/game1/pic1"],
            //             "type":[0,1,2],
            //             "minPlayer":6,
            //             "maxPlayer":10,
            //             "description":""
            //         }, {
            //             "index":782193789122,
            //             "name":"LANLENSHA",
            //             "englishName":"sadasdas",
            //             "images":["/images/game1/pic1"],
            //             "type":[0,1,2],
            //             "minPlayer":9,
            //             "maxPlayer":12,
            //             "description":""
            //         }]
            //     }
            // });
        }
    } else {
        res.status(404).end();
    }
});

app.route('/api/v0/:action')
.get((req, res) => {
    res.type('application/json');
    res.send({
        'reply': 'no-auth',
        'method': req.method,
        'action': req.params.action
    });
})
.post((req, res) => {
    res.type('application/json');
    if (req.header['x-subdomain'] == 'api') {
        res.send({
            'reply': 'success',
            'method': req.method,
            'action': req.params.action
        });
    } else {
        res.status(401).send({
            'reply': 'no-auth',
            'method': req.method,
            'action': req.params.action
        });
    }
});

app.get('*', (req, res) => {
    res.status(404).end();
    // res.sendFile('404.html', {'root': __dirname});
});

const now = (arg) => arg == 'u' ? Math.round(new Date().getTime() / 1000.0) : new Date().toLocaleString(undefined, {hour12: false});
const ipdefined = (mIp) => mIp && mIp.replace('::ffff:', '');

process.stdin.setEncoding('utf-8');
process.stdin.on('data', (data) => {
    let inputbuffer = data.toString().trim();
    inputbuffer = inputbuffer.substring(inputbuffer.indexOf('\n') + 1);
    console.log('>:', inputbuffer);
    let arr = inputbuffer.split(' ');

    if (arr[0] === '/session') {
        console.log(sessStore.sessions);
    }

    if (arr[0] == '/get') {
        let page = arr[1] || 1;
        qqdb.getList(page).then((data) => {
            console.log(data);
        });
    }
    if (arr[0] == '/add') {
        qqdb.addList(['QID' + now('u'), '阿瓦隆', 'awalong', '1,2,3', 1, 6, '說明這是一個阿瓦隆']).then((data) => {
            console.log(data);
        });;
    }


    if (arr[0] == '/gettable') {
        qqdb.getTable().then((data) => {
            console.log(data);
        });
    }
});

