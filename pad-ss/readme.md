# pad-ss

#### At extlist url in json  
+ append mons_*.bc and 3 digits at least
+ append cards_*.bc and 3 digits

#### Try some test format in extlist.bin  
+ header (16): ['mons_count', 'I'] (4) / ['cards_count', 'I'] (4) / ['magic_string', '4s'] (4) / ['None', '4x'] (4)
+ mons per count (24): ['id_number', 'H'] (2) / ['None', '4x'] (4) / ['None', '4x'] (4) / ['None', '4x'] (4) / ['None', '2x'] (2) / ['uncompressed_size', 'I'] (4) / ['None', '4x'] (4)
+ card per count (24): ['id_number', 'B'] (1) / ['None', 'x'] (1) / ['None', '4x'] (4) / ['None', '4x'] (4) / ['None', '4x'] (4) / ['None', '2x'] (2) / ['uncompressed_size', 'I'] (4) / ['None', '4x'] (4)
+ Compressed Asset? (8): ['compressed_size', 'I'] (4) / ['None', '4x'] (4)

# miru-bot
Q^Q  
[mirubot-data](https://console.cloud.google.com/storage/browser/mirubot-data/)  

## Resource
[JP, Android API](http://dl.padsv.gungho.jp/base_adr.json)  
[JP, IOS API](http://dl.padsv.gungho.jp/base.json)  
[CH, iOS API](http://dl.padsv.gungho.jp/base.ht-ios.json)  
[KR, iOS API](http://patch-kr-pad.gungho.jp/base.kr-ios.json)  
[PAD Dev Resources](https://pad.protic.site/blog/pad-dev-resources/)  
[How does PADx and Padguide acquire their monster and dungeon data?](https://www.reddit.com/r/PuzzleAndDragons/comments/7ly4x7/how_does_padx_and_padguide_acquire_their_monster/)  
[Some pad.bc data](https://www.vg-resource.com/thread-27573-page-3.html)  