const fs = require('fs');
const path = require('path');
const {Storage} = require('@google-cloud/storage');

const key_file = path.join(__dirname, 'skey/Project-01-pad-ss-82e30c4e7f22.json');
const version_file = path.join(__dirname, 'raw_data/version.json');
const card_data_file = path.join(__dirname, 'raw_data/download_card_data.json');
const skill_data_file = path.join(__dirname, 'raw_data/download_skill_data.json');
const monster_parse_data_path = path.join(__dirname, 'monster_parse/');
const skill_parse_data_path = path.join(__dirname, 'skill_parse/');

const GCS_storage = new Storage({keyFilename: key_file});
const GCS_bucket = 'mirubot-data';
const GCS_filepath = 'paddata/raw/jp/';
const GCS_card_data = 'download_card_data.json';
const GCS_skill_data = 'download_skill_data.json';

let data_version = {
    'card': { 'updated': 'null' },
    'skill': { 'updated': 'null' }
};

let SkillDatabase, MonsterDatabase, parseMonster = 0, skipMonster = 0;


process.stdout.write('\033c');
// loadVersion(); // Start
// loadRawMonsterData();
loadRawSkillData();

// ### Check database start
/*
*   1. load version.json & get two data modify date = version
*   2. get google cloud storage data modify time to check
*   3. if local version empty or different then GCS version, download it
*   ps. https://console.cloud.google.com/storage/browser/mirubot-data/paddata/raw/jp/
*/
function loadVersion() {
    fs.open(version_file, 'a+', (err, fd) => {
        if (err) {
            console.error('[Local] Open file error:\n', err);
        } else {
            let buff = Buffer.alloc(1024);
            fs.read(fd, buff, 0, buff.length, 0, (err, bytes, buff) => {
                if (err) {
                    console.error('[Local] Read file error:\n', err);
                } else {
                    if (bytes <= 0) {
                        fs.write(fd, JSON.stringify(data_version, null, 4), (err, wt, str) => {
                            console.error('[Local] Write file error:\n', err);
                            fs.close(fd, (err) => {
                                if (err) console.error('[Local] Close file error:\n', err);
                                loadVersionFin();
                            });
                        });
                    } else {
                        data_version = JSON.parse(buff.slice(0, bytes).toString());
                        fs.close(fd, (err) => {
                            if (err) console.error('[Local] Close file error:\n', err);
                            loadVersionFin();
                        });
                    }
                }
            });
        }
    });
}
function loadVersionFin() {
    console.log('[Local] Data version: (Monster)', data_version['card']['updated'], ', (Skill)', data_version['skill']['updated'], '.');
    loadMetadata().catch(console.error).then(checkVersion);
}
async function loadMetadata() {
    console.log('[Cloud] Check metadata:', GCS_card_data, '.');
    const [meta_card] = await GCS_storage.bucket(GCS_bucket).file(GCS_filepath + GCS_card_data).getMetadata();
    console.log('[Cloud] Check metadata:', GCS_skill_data, '.');
    const [meta_skill] = await GCS_storage.bucket(GCS_bucket).file(GCS_filepath + GCS_skill_data).getMetadata();

    return {
        'card': {
            'file': meta_card['name'],
            'size': meta_card['size'],
            'updated': meta_card['updated'],
            'md5Hash': meta_card['md5Hash'],
            'link': meta_card['selfLink'],
        },
        'skill': {
            'file': meta_skill['name'],
            'size': meta_skill['size'],
            'updated': meta_skill['updated'],
            'md5Hash': meta_skill['md5Hash'],
            'link': meta_skill['selfLink'],
        }
    };
}
function checkVersion(metadata) {
    let modify = false;
    let meta_card = metadata['card'];
    let meta_skill = metadata['skill'];
    let download = [];
    if (data_version['card']['updated'] == '' || data_version['card']['updated'] != meta_card['updated']) {
        modify = true;
        data_version['card'] = meta_card;
        download.push([GCS_filepath + GCS_card_data, card_data_file]);
    }

    if (data_version['skill']['updated'] == '' || data_version['skill']['updated'] != meta_skill['updated']) {
        modify = true;
        data_version['skill'] = meta_skill;
        download.push([GCS_filepath + GCS_skill_data, skill_data_file]);
    }
    downloadGCSData(download).catch(console.error).then(() => {
        downloadGCSDataFin(modify);
    });
}
async function downloadGCSData(list) {
    if (list.length > 0) {
        for (let idx = 0; idx < list.length; idx++) {
            console.log(`[Cloud] Start download: gs://${GCS_bucket}/${list[idx][0]} to ${list[idx][1]} ...`);
            let options = {
                'destination': list[idx][1]
            };
            await GCS_storage.bucket(GCS_bucket).file(list[idx][0]).download(options);
        }
    }
    return false;
}
function downloadGCSDataFin(isModify) {
    if (isModify) {
        console.log('[Cloud] Download finish.');
        fs.writeFile(version_file, JSON.stringify(data_version, null, 4), (err) => {
            if (err) {
                console.log('[Local] Update version.json error.\n', err);
            } else {
                console.log('[Local] Data version check finish.');
                loadRawSkillData();
            }
        });
    } else {
        console.log('[Local] Data version is up to date.');
    }
}
// ### Check database end

// ### Parser database start
/*  1. load download_skill_data.json & parser data to global
*   2. load download_card_data.json & parser data & link skill data -> monster_parse/0001.json
*   2.a. monster_parse/0001.json = [
        id, name, attr, subattr, [...type], [hp, atk, rcv], limitbreak_rate,
        awaken, [..sawaken], [...active_skill], leader_skill
    ]
*   2.b. [...active_skill] = [name, desc, min_turn, max_turn]


*   3. keep monster id & active skill tag -> monster_link_skill.json
*   4. take skill id & skill tag -> skill_link_tag.json
*/
function loadRawSkillData() {
    fs.readFile(skill_data_file, (err, data) => {
        if (err) {
            console.error('[Local] Open file error:\n', err);
        } else {
            loadRawSkillDataFin(JSON.parse(data)['skill']);
        }
    });
}
function loadRawSkillDataFin(data) {
    SkillDatabase = data;
    console.log('[Local] Load skill data finish. (' + data.length + ')');
    loadRawMonsterData();
}
function loadRawMonsterData() {
    fs.readFile(card_data_file, (err, data) => {
        if (err) {
            console.error('[Local] Open file error:\n', err);
        } else {
            loadRawMonsterDataFin(JSON.parse(data)['card']);
        }
    });
}
function loadRawMonsterDataFin(data) {
    MonsterDatabase = data;
    console.log('[Local] Load monster data finish. (' + data.length + ')');
    parserMonsterData();
}
function parserMonsterData() {
    if (parseMonster < MonsterDatabase.length) {
        let monster = new MonsterData(MonsterDatabase[parseMonster]);
        if (monster['id'] > 0 && monster['id'] < 100000) {
            fs.writeFileSync(path.join(monster_parse_data_path, ('000' + monster.id).slice(-4) + '.json'), monster.toJSON());

            if (monster['active_skill_id'] == 0) {
                console.log('\n\ntag: [ null ]');
            } else {
                console.log('\n\nmon:', monster['id'], monster['name'], monster['active_skill_id']);
                console.log('tag:', new SkillTagData(SkillDatabase[monster['active_skill_id']]).defindTag());
                // new SkillTagData(SkillDatabase[monster['active_skill_id']]).defindTag();
            }
        } else {
            skipMonster++;
        }
        // process.stdout.write('\r[Local] Parse monster data (' + parseMonster + ' / ' + MonsterDatabase.length + ') completed.');
        console.log(`[Local] Parse monster data (${parseMonster + 1} / ${MonsterDatabase.length}) completed.`);

        parseMonster++;
        parserMonsterData();
    } else {
        console.log(`\n[Local] Parse monster data finish. (${MonsterDatabase.length} - ${skipMonster + 1} = ${MonsterDatabase.length - skipMonster - 1})`);
    }
}

// ### Parser database end ### ///
class MonsterData {
    constructor(mRaw) {
        this.id = mRaw[0];
        this.name = mRaw[1];
        this.attribute = attribute(mRaw[2]);
        this.subattribute = attribute(mRaw[3]);
        // 4 is_ultimate in Evolution # True if ultimate, False if normal evo.
        this.types = [type(mRaw[5])];
        this.types.push(type(mRaw[6]));
        // this.rarity = mRaw[7];
        // 8 this.cost = mRaw[8];
        // 9 width of enemy # If 5, the monster always spawns alone.
        // this.max_level = mRaw[10];
        // 11 this.feed_experience_per_level = mRaw[11] / 4;
        // 12 this.released = mRaw[12] == 100;
        // 13 this.sell_value_coin_per_level = mRaw[13] / 10;
        // this.hp_min = mRaw[14];
        this.hp_max = mRaw[15];
        // this.hp_scale = mRaw[16];
        // this.atk_min = mRaw[17];
        this.atk_max = mRaw[18];
        // this.atk_scale = mRaw[19];
        // this.rcv_minimum = mRaw[20];
        this.rcv_max = mRaw[21];
        // this.rcv_scale = mRaw[22];
        // 23 this.exp_max = mRaw[23];
        // 24 this.exp_scale = mRaw[24];
        this.active_skill_id = mRaw[25]; // !! skill label
        this.active_skill = new SkillData(mRaw[25], SkillDatabase[mRaw[25]]); // !! skill label
        this.leader_skill_id = mRaw[26]; // !! skill label
        // 27 - 39 stats in Enemy
        // 40 - 50 Evolution
        // 51 - 57 skills in Enemy
        let enemy_skill_count = mRaw[57];
        let i = 3 * enemy_skill_count;
        let awaken_count = mRaw[i + 58];
        this.awakens = [];
        for (let a = 0; a < awaken_count; a++) {
            this.awakens.push(mRaw[i + 59 + a]);
        }
        let j = i + awaken_count;
        this.super_awakens = [];
        mRaw[j + 59].split(',').forEach((elem) => {
            if (elem != '')
                this.super_awakens.push(elem);
        });
        // 60 + j Base Evo Id
        // 61 + j Group
        this.types.push(type(mRaw[j + 62]));
        this.types = this.types.filter((t) => { return t != 'NONE'; });
        // 63 + j Sell Value MP
        // 64 + j Latent Awaken
        // 65 + j Collaborate
        // 66 + j some Flag
        // 67 + j Furigana
        this.limitbreak_stat_increase = mRaw[j + 68] / 100;
        // 69 + j Skill Voice
        // 70 + j Orb Skin
        // 71 + j Link
    }

    toJSON () {
        return JSON.stringify([
            this.id, this.name, this.attribute, this.subattribute, this.types,
            [this.hp_max, this.atk_max, this.rcv_max], this.limitbreak_stat_increase,
            this.awakens, this.super_awakens,
            this.active_skill.toSimpleJSON(), this.leader_skill_id
        ]);
    }
}

class SkillData {
    constructor(mId, mRaw) {
        this.id = mId;
        this.name = mRaw[0];
        this.description = mRaw[1];
        this.type = mRaw[2];
        this.level = mRaw[3];
        this.maxturn = mRaw[4];
        this.minturn = mRaw[4] - (mRaw[3] - 1);
        // this.unknow = mRaw[5];
        this.tag = mRaw.slice(6);
    }

    toSimpleJSON () {
        return [this.name, this.description, this.minturn, this.maxturn];
    }

    toJSON () {
        return [
            this.id, this.name, this.description, this.type, this.level,
            [this.maxturn, this.minturn], this.data
        ];
    }
}

class SkillTagData {
    constructor(mRaw) {
        // this.name = mRaw[0]; // ""
        // this.description = mRaw[1]; // ""
        this.type = mRaw[2]; // skill tag
        // this.level = mRaw[3]; // 0
        // this.maxturn = mRaw[4]; // 0
        // this.unknow = mRaw[5]; // ??
        this.data = mRaw.slice(6);
    }

    defindTag () {
        let tag = [];
        if (this.type == '0') { // 全體射擊 固定屬性 倍率攻撃
            tag.push('SHOOT', 'SHOOT_MASS', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_MULTI');
        } else if (this.type == '1') { // 全體射擊 固定屬性 點數攻撃
            tag.push('SHOOT', 'SHOOT_MASS', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_STATIC');
        } else if (this.type == '2') { // 單體射擊 自身屬性 倍率攻撃
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_SELFATTR', 'SHOOT_MULTI');
        } else if (this.type == '3') { // 減傷盾 傷害減傷
            tag.push('SHIELD', 'SHIELD_REDUCE');
        } else if (this.type == '4') { // 毒
            tag.push('POISON');
        } else if (this.type == '5') { // CTW
            tag.push('CHANGE_THE_WORLD');
        } else if (this.type == '6') { // 重力
            tag.push('GRAVITY');
        } else if (this.type == '7') { // 血量回復 倍率回復
            tag.push('HP_RECOVER', 'HP_RECOVER_MULTI');
        } else if (this.type == '8') { // 血量回復 點數回復
            tag.push('HP_RECOVER', 'HP_RECOVER_STATIC');
        } else if (this.type == '9') { // 單色珠轉 轉色
            tag.push('ORB_CHANGE', 'ORB_FROM_' + orb(this.data[0]));
            tag.push('ORB_TO_' + orb(this.data[1] || '0'));
        } else if (this.type == '10') { // 落珠洗板
            tag.push('BOARD_REFRESH');
        } else if (this.type == '18') { // 威嚇
            tag.push('DELAY');
        } else if (this.type == '19') { // 防禦力減少 (全減)
            tag.push('DEFENSE_REDUCE');
            if (skillmulti(this.data[1]) == 1) {
                tag.push('DEFENSE_REDUCE_ALL');
            }
        } else if (this.type == '20') { // 雙色珠轉 轉(單/雙)色
            tag.push('ORB_CHANGE', 'ORB_FROM_' + orb(this.data[0]));
            tag.push('ORB_FROM_' + orb(this.data[2]));
            if ((this.data[3] && this.data[1] == this.data[3]) || (!this.data[3] && this.data[1] == 0)) {
                tag.push('ORB_TO_' + orb(this.data[1]));
            } else {
                tag.push('ORB_TO_' + orb(this.data[1]));
                tag.push('ORB_TO_' + orb(this.data[3] || '0'));
            }
        } else if (this.type == '21') { // 減傷盾 屬性減傷
            tag.push('SHIELD', 'SHIELD_ATTR');
        } else if (this.type == '35') { // 單體射擊 自身屬性 倍率攻撃 吸血
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_SELFATTR', 'SHOOT_MULTI', 'SHOOT_RECOVER');
        } else if (this.type == '37') { // 單體射擊 固定屬性 倍率攻撃
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_MULTI');
        } else if (this.type == '42') { // 敵屬性射擊 固定屬性 點數攻撃
            tag.push('SHOOT', 'SHOOT_ENEMYATTR', 'SHOOT_' + attribute(this.data[1]), 'SHOOT_STATIC');
        } else if (this.type == '50') { // 單項屬性傷害(回復力)強化
            tag.push('ENHANCE');
            if (this.data[1] == '5') {
                tag.push(attribute(this.data[1]) + '_ENHANCE');
            } else {
                tag.push(attribute(this.data[1]) + '_ATK_ENHANCE');
            }
        } else if (this.type == '51') { // 全體攻擊
            tag.push('MASS_ATTACK');
        } else if (this.type == '52') { // 單屬性點燈
            tag.push('ORB_ENHANCE', orb(this.data[0]) + '_ORB_ENHANCE');
        } else if (this.type == '55') { // 單體射擊 真實傷害
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_TRUE');
        } else if (this.type == '56') { // 全體射擊 真實傷害
            tag.push('SHOOT', 'SHOOT_MASS', 'SHOOT_TRUE');
        } else if (this.type == '58') { // 全體射擊 固定屬性 隨機傷害
            tag.push('SHOOT', 'SHOOT_MASS', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_RANDOM');
        } else if (this.type == '59') { // 全體射擊 固定屬性 隨機傷害
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_RANDOM');
        } else if (this.type == '60') { // 反撃
            tag.push('COUNTER');
        } else if (this.type == '71') { // 全版轉色
            tag.push('ORB_CHANGE', 'ORB_FROM_ALL');
            for (let idx = 0, len = this.data.length; idx < len; idx++) {
                if (this.data[idx] != -1) {
                    tag.push('ORB_TO_' + orb(this.data[idx]));
                }
            }
        } else if (this.type == '84') { // 單體射擊 固定屬性 倍率攻撃 自殘
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_MULTI', 'SHOOT_HURT');
        } else if (this.type == '85') { // 全體射擊 固定屬性 倍率攻撃 自殘
            tag.push('SHOOT', 'SHOOT_MASS', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_MULTI', 'SHOOT_HURT');
        } else if (this.type == '86') { // 單體射擊 固定屬性 點數攻撃 自殘
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_STATIC', 'SHOOT_HURT');
        } else if (this.type == '87') { // 全體射擊 固定屬性 點數攻撃 自殘
            tag.push('SHOOT', 'SHOOT_MASS', 'SHOOT_' + attribute(this.data[0]), 'SHOOT_STATIC', 'SHOOT_HURT');
        } else if (this.type == '88') { // 單項類型傷害強化
            tag.push('ENHANCE', type(this.data[1]) + '_TYPE_ENHANCE');
        } else if (this.type == '90') { // 雙項屬性傷害(回復力)強化
            tag.push('ENHANCE');
            if (this.data[1] == '5') {
                tag.push(attribute(this.data[1]) + '_ENHANCE');
            } else {
                tag.push(attribute(this.data[1]) + '_ATK_ENHANCE');
            }
            if (this.data[2] == '5') {
                tag.push(attribute(this.data[2]) + '_ENHANCE');
            } else {
                tag.push(attribute(this.data[2]) + '_ATK_ENHANCE');
            }
        } else if (this.type == '91') { // 雙屬性點燈
            tag.push('ORB_ENHANCE');
            tag.push(orb(this.data[0]) + '_ORB_ENHANCE');
            tag.push(orb(this.data[1]) + '_ORB_ENHANCE');
        } else if (this.type == '92') { // 雙項類型傷害強化
            tag.push('ENHANCE');
            tag.push(type(this.data[1]) + '_TYPE_ENHANCE');
            tag.push(type(this.data[2]) + '_TYPE_ENHANCE');
        } else if (this.type == '93') { // 隊長轉換
            tag.push('LEADER_SWAP');
        } else if (this.type == '110') { // 血量倍率射擊
            tag.push('SHOOT', 'SHOOT_MULTI', 'SHOOT_HP_PERCENT');
            if (this.data[0] == 0) {
                tag.push('SHOOT_MASS');
            } else {
                tag.push('SHOOT_SINGLE');
            }
            tag.push('SHOOT_' + attribute(this.data[1]));
        } else if (this.type == '115') { // 單體射擊 固定屬性 倍率攻撃 吸血
            tag.push('SHOOT', 'SHOOT_SINGLE', 'SHOOT_MULTI', 'SHOOT_RECOVER');
            tag.push('SHOOT_' + attribute(this.data[0]));
        } else if (this.type == '116') { // 多功能技能類性
            for (let idx = 0, len = this.data.length; idx < len; idx++) {
                tag.push(...(new SkillTagData(SkillDatabase[this.data[idx]]).defindTag()));
            }
        } else if (this.type == '117') { // 解綁 回血 解除覺醒無效
            if (this.data[0]) {
                tag.push('BIND_RECOVER');
                if (this.data[0] == 9999) tag.push('BIND_RECOVER_ALL');
            }
            if (this.data[1]) tag.push('HP_RECOVER', 'HP_RECOVER_MULTI');
            if (this.data[2]) tag.push('HP_RECOVER', 'HP_RECOVER_STATIC');
            if (this.data[3]) tag.push('HP_RECOVER', 'HP_RECOVER_PERCENT');
            if (this.data[4]) {
                tag.push('AWAKEN_BIND_RECOVER');
                if (this.data[4] == 9999) tag.push('AWAKEN_BIND_RECOVER_ALL');
            }
        } else if (this.type == '') {
            tag.push('');
        } else if (this.type == '') {
            tag.push('');
        } else {
            console.log('\n[Error] Skill tag not defind type:', this.type);
            return '';
        }
        return tag;
    }
}

function attribute(mAttr) {
    return {
        '-1': 'NONE',   // 無
        '0': 'FIRE',    // 火屬性
        '1': 'WATER',   // 水屬性
        '2': 'WOOD',    // 木屬性
        '3': 'LIGHT',   // 光屬性
        '4': 'DARK',    // 暗屬性
        '5': 'RECOVER'  // 回復力
    }[mAttr];
}

function type(mType) {
    return {
        '-1': 'NONE',           // 無
        '0': 'EVO_MATERIAL',    // 進化類型
        '1': 'BALANCED',        // 平衡類型
        '2': 'PHYSICAL',        // 體力類型
        '3': 'HEALER',          // 回復類型
        '4': 'DRAGON',          // 龍類型
        '5': 'GOD',             // 神類型
        '6': 'ATTACKER',        // 攻擊類型
        '7': 'DEVIL',           // 惡魔類型
        '8': 'MACHINE',         // 機械類型
        '12': 'AWAKEN_MATERIAL', // 能力覚醒類型
        '14': 'ENHANCE_MATERIAL', // 強化合成類型
        '15': 'REDEEMABLE_MATERIAL' // 売却類型
    }[mType];
}

function orb (mOrb) {
    return {
        '-1': 'NONE',
        '0': 'FIRE',
        '1': 'WATER',
        '2': 'WOOD',
        '3': 'LIGHT',
        '4': 'DARK',
        '5': 'HEAL',
        '6': 'JAMMER',
        '7': 'POISON',
        '8': 'MORTAL_POISON',
        '9': 'BOMB'
    }[mOrb];
}

function skillmulti(x) {
    return x / 100 || 0;
}

function awaken(mAwaken) {
    let awaken = {
        '0': 'NONE',
        '1': 'ENHANCED_HP',
        '2': 'ENHANCED_ATTACK',
        '3': 'ENHANCED_RECOVERY',
        '4': 'REDUCE_FIRE_DAMAGE',
        '5': 'REDUCE_WATER_DAMAGE',
        '6': 'REDUCE_WOOD_DAMAGE',
        '7': 'REDUCE_LIGHT_DAMAGE',
        '8': 'REDUCE_DARK_DAMAGE',
        '9': 'AUTO_RECOVER',
        '10': 'RESISTANCE_BIND',
        '11': 'RESISTANCE_DARK',
        '12': 'RESISTANCE_JAMMER',
        '13': 'RESISTANCE_POISON',
        '14': 'ENHANCED_FIRE_ORBS',
        '15': 'ENHANCED_WATER_ORBS',
        '16': 'ENHANCED_WOOD_ORBS',
        '17': 'ENHANCED_LIGHT_ORBS',
        '18': 'ENHANCED_DARK_ORBS',
        '19': 'EXTEND_TIME',
        '20': 'RECOVER_BIND',
        '21': 'SKILL_BOOST',
        '22': 'ENHANCED_FIRE_ATT',
        '23': 'ENHANCED_WATER_ATT',
        '24': 'ENHANCED_WOOD_ATT',
        '25': 'ENHANCED_LIGHT_ATT',
        '26': 'ENHANCED_DARK_ATT',
        '27': 'TWO_PRONGED_ATTACK',
        '28': 'RESISTANCE_SKILL_BIND',
        '29': 'ENHANCED_HEAL_ORBS',
        '30': 'MULTI_BOOST',
        '31': 'DRAGON_KILLER',
        '32': 'GOD_KILLER',
        '33': 'DEVIL_KILLER',
        '34': 'MACHINE_KILLER',
        '35': 'BALANCED_KILLER',
        '36': 'ATTACKER_KILLER',
        '37': 'PHYSICAL_KILLER',
        '38': 'HEALER_KILLER',
        '39': 'AWAKEN_KILLER',
        '40': 'ENHANCE_KILLER',
        '41': 'REDEEMABLE_KILLER',
        '42': 'EVO_KILLER',
        '43': 'ENHANCED_COMBOS',
        '44': 'GUARD_BREAK',
        '45': 'BONUS_ATTACK',
        '46': 'ENHANCED_TEAM_HP',
        '47': 'ENHANCED_TEAM_RECOVERY',
        '48': 'VOID_DAMAGE_PIERCER',
        '49': 'AWOKEN_ASSIST',
        '50': 'SUPER_BONUS_ATTACK',
        '51': 'SKILL_CHARGE',
        '52': 'RESISTANCE_BIND_PLUS',
        '53': 'EXTENDED_MOVE_TIME_PLUS',
        '54': 'RESISTANCE_CLOUDS',
        '55': 'RESISTANCE_IMMOBILITY',
        '56': 'SKILL_BOOST_PLUS',
        '57': 'HP_80_OR_MORE_ENHANCED',
        '58': 'HP_50_OR_LESS_ENHANCED',
        '59': 'L_DAMAGE_REDUCTION',
        '60': 'L_INCREASED_ATTACK',
        '61': 'SUPER_ENHANCED_COMBOS',
        '62': 'COMBO_ORBS',
        '63': 'SKILL_VOICE',
        '64': 'DUNGEON_BONUS',
        '65': 'REDUCED_HP',
        '66': 'REDUCED_ATTACK',
        '67': 'REDUCED_RECOVERY',
        '68': 'RESISTANCE_DARK+',
        '69': 'RESISTANCE_JAMMER+',
        '70': 'RESISTANCE_POISON+',
        '71': 'BLESSING_JAMMERS',
        '72': 'BLESSING_POISON' // 毒ドロップの加護
    };
    return awaken[mAwaken] || console.log(awaken);
}
function allTag() {
    let all = [
        'SHOOT', 'SHOOT_MASS', 'SHOOT_SINGLE',
        'SHOOT_FIRE', 'SHOOT_WATER', 'SHOOT_WOOD', 'SHOOT_LIGHT', 'SHOOT_DARK',
        'SHOOT_SELFATTR', 'SHOOT_ENEMYATTR', 'SHOOT_RECOVER', 'SHOOT_HURT',
        'SHOOT_MULTI', 'SHOOT_STATIC', 'SHOOT_TRUE', 'SHOOT_RANDOM', 'SHOOT_HP_PERCENT',
        'SHIELD', 'SHIELD_REDUCE', 'SHIELD_ATTR',
        'POISON',
        'CHANGE_THE_WORLD',
        'GRAVITY',
        'HP_RECOVER', 'HP_RECOVER_MULTI', 'HP_RECOVER_STATIC',
        'ORB_CHANGE', 'ORB_FROM_ALL', 'ORB_FROM_FIRE', 'ORB_FROM_WATER', 'ORB_FROM_WOOD', 'ORB_FROM_LIGHT', 'ORB_FROM_DARK',
        'ORB_FROM_HEAL', 'ORB_FROM_JAMMER', 'ORB_FROM_POISON', 'ORB_FROM_MORTAL_POISON', 'ORB_FROM_BOMB',
        'ORB_TO_FIRE', 'ORB_TO_WATER', 'ORB_TO_WOOD', 'ORB_TO_LIGHT', 'ORB_TO_DARK',
        'ORB_TO_HEAL', 'ORB_TO_JAMMER', 'ORB_TO_POISON', 'ORB_TO_MORTAL_POISON', 'ORB_TO_BOMB',
        'ENHANCE', 'RECOVER_ENHANCE', 'FIRE_ATK_ENHANCE', 'WATER_ATK_ENHANCE', 'WOOD_ATK_ENHANCE', 'LIGHT_ATK_ENHANCE', 'DARK_ATK_ENHANCE',
        'BALANCED_TYPE_ENHANCE', 'PHYSICAL_TYPE_ENHANCE', 'HEALER_TYPE_ENHANCE', 'DRAGON_TYPE_ENHANCE', 
        'GOD_TYPE_ENHANCE', 'ATTACKER_TYPE_ENHANCE', 'DEVIL_TYPE_ENHANCE', 'MACHINE_TYPE_ENHANCE', 
        'MASS_ATTACK',
        'ORB_ENHANCE', 'FIRE_ORB_ENHANCE', 'WATER_ORB_ENHANCE', 'WOOD_ORB_ENHANCE', 'LIGHT_ORB_ENHANCE', 'DARK_ORB_ENHANCE', 'RECOVER_ORB_ENHANCE',
        'COUNTER', 'LEADER_SWAP',
        '', '',
        '', '', '',
        '', '', '',
    ];
}

/*
// ** Terminal Input ** //
process.stdin.on('data', (data) => {
    let inputbuffer = data.toString().trim();
    inputbuffer = inputbuffer.substring(inputbuffer.indexOf('\n') + 1);
    console.log('>:', inputbuffer);
    let array = inputbuffer.split(' ');

    // Let the client escape
    if (array[0] === 'exit') { return cleanup(); }
    if (array[0] === 'quit') { return cleanup(); }
});
*/

