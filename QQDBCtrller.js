const sqlite3 = require('sqlite3').verbose();
const dbfile = './database.db'
const now = () => new Date().toLocaleString(undefined, {hour12: false});

const ListsPerPage = 10;

class QQDBCtrller {
    constructor () {
        this.db = new sqlite3.Database(dbfile);
        this.db.serialize(() => {
            console.info(now(), '[DB] init');
            this.db.run('CREATE TABLE IF NOT EXISTS GameList(' +
            'QID TEXT UNIQUE, ' +
            'Name TEXT, ' +
            'SubName TEXT, ' +
            'Tag TEXT, ' +
            'MinPlayer INTEGER, ' +
            'MaxPlayer INTEGER, ' +
            'Description TEXT ' +
            ');');
        });
    }

    getListPage () {
        return new Promise((resolve, reject) => {   
            let stmt = this.db.prepare('SELECT MAX(rowid) id FROM GameList LIMIT 1;');
            stmt.get([], (err, result) => {
                if (err) {
                    console.error(now(), '[DB] getListPage', err);
                    reject(err);
                } else {
                    let page = Math.ceil(result['id'] / ListsPerPage);
                    resolve(page);
                }
            });
            stmt.finalize();
        });
    }

    getList (args) {
        return new Promise((resolve, reject) => {
            let stmt = this.db.prepare('SELECT * FROM GameList LIMIT ? OFFSET ?;');
            stmt.all([ListsPerPage, ListsPerPage * (args - 1)], (err, result) => {
                if (err) {
                    console.error(now(), '[DB] getList', err);
                    reject(err);
                } else {
                    resolve(result)
                }
            });
            stmt.finalize();
        });
    }

    addList (arg) {
        return new Promise((resolve, reject) => {
            this.db.serialize(() => {
                let stmt = this.db.prepare('INSERT INTO GameList VALUES (?, ?, ?, ?, ?, ?, ?)');
                stmt.run(arg, (err) => {
                    if (err) {
                        console.error(now(), '[DB] addList', err);
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
                stmt.finalize();
        
                this.db.run('CREATE TABLE IF NOT EXISTS ' + arg[0] + '(' +
                'Type TEXT, ' +
                'Title TEXT, ' +
                'Image TEXT, ' +
                'Description TEXT, ' +
                'Keyword TEXT ' +
                ');');
            });
        });
    }




    // Debug !!

    getTable () {
        return new Promise((resolve, reject) => {
            let stmt = db.prepare('SELECT * FROM sqlite_master;');
            stmt.all([], (err, result) => {
                if (err) {
                    console.error(now(), '[DB] getTable', err);
                    reject(err);
                } else {
                    resolve(result)
                }
            });
            stmt.finalize();
        });
    }
}

module.exports = QQDBCtrller;

