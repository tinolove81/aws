# SomethingWas.tw
Create a personal web server

## Tool
0. [AWS EC2](https://ap-northeast-1.console.aws.amazon.com/ec2/v2/home) 伺服器架設位置，A3
0. [Godaddy](https://dcc.godaddy.com/domains) 網域管理，G3

## Usage
### Terminal

開啟背景執行
>```
>$ screen
>```

離開背景執行(未關閉)
>按 `Ctrl` + `a` 後，按 `d` 跳出

檢視所有背景執行
>```
>$ screen -ls
>```

回復背景執行
>```
>$ screen -r [process]
>```

離開背景執行(關閉)
>```
>$ exit
>```

### git相關
>```
>$ git status
>$ git fetch
>$ git pull
>$ sync
>$ vim file.js
>$ git add file.js
>$ git commit -m 'description'
>$ git push
>```

### node相關
>```
>$ cd ../Tinolove81/aws
>$ sudo node index.js
>$ vim index.js
>$ cat index.js
>```

## Description
記錄用